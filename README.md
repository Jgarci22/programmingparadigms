### ProgrammingParadigms
 Group Project 2 - Android
 Project members: Jorge Garcia (jgarci22), Jamie Zhang (jzhang17)

#### Edited Files:
>   1. **activity_main.xml** (HelloJorgeJamie/app/res/layout/activity_main.xml)
>   2. **main.xml** (HelloJorgeJamie/app/res/menu/main.xml)
>   3. **strings.xml** (HelloJorgeJamie/app/res/values/strings.xml)
>   4. **MainActivity.java** (HelloJorgeJamie/app/java/com.example.hellojorgejamie/MainActivity.java)
>   5. **AndroidManifest.xml** (HelloJorgeJamie/app/manifests/AdroidManifest.xml)
>   6. **NetworkUtils.java** (HelloJorgeJamie/app/java/com.example.hellojorgejamie/utilities/NetworkUtils.java)


To run the android app, checkout the **LatestChanges** branch. Inside **LatestChanges**,
there is a folder named **HelloJorgeJamie** which contains all the files, functions, 
methods needed to run the android app. Download **HelloJorgeJamie** to the desired path
and then you can open the folder using Android Studio.